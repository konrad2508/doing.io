const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const config = require('../config');
const userService = require('./user');

module.exports = {
  async checkCredentials(username, password) {
    const user = await userService.readUser(username);
    const validPassword = user && await bcrypt.compare(password, user.passwordHash);
    return { user, validPassword };
  },
  authUser(user) {
    const userForToken = {
      username: user.username,
      id: user._id,
    };
    return jwt.sign(userForToken, config.SECRET);
  },
};
