const { isValid } = require('mongoose').Types.ObjectId;

const validObjectId = (str) => isValid(str);

module.exports = { validObjectId };
