import axios from 'axios';

import { User, UserToken } from '../types';
import { SERVER_ADDRESS } from '../config';

const BASE_URL = `${SERVER_ADDRESS}/auth`;
const getToken = () => localStorage.getItem('user_token');

const login = (username: string, password: string): Promise<UserToken> => axios
  .post(`${BASE_URL}/login`, { username, password })
  .then((r) => r.data as UserToken);

const me = (): Promise<User> => axios
  .get(`${BASE_URL}/me`, { headers: { Authorization: getToken() ? `bearer ${getToken()}` : '' } })
  .then((r) => r.data as User);

export default {
  login,
  me,
};
