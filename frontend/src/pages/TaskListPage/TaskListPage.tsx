import React, { useEffect, useState } from 'react';
import {
  Dimmer, Header, Icon, Loader,
} from 'semantic-ui-react';
import { useHistory, useParams } from 'react-router-dom';

import { TaskList } from '../../types';
import TaskListBar from './TaskListBar';
import taskListService from '../../services/taskListService';

const TaskListPage: React.FC = () => {
  const history = useHistory();
  const { id } = useParams();
  const [dimmerActive, setDimmerActive] = useState<boolean>(false);
  const [taskList, setTaskList] = useState<TaskList | null>(null);

  useEffect(() => {
    taskListService
      .getTaskList(id)
      .then((res) => setTaskList(res))
      .catch(() => history.push('/'));
  }, [id, setTaskList, history]);

  const finalize = () => {
    if (taskList == null) return;
    taskListService
      .updateTaskList({ ...taskList, isFinalized: true })
      .then((res) => {
        setTaskList(res);
        setDimmerActive(true);
      });
  };

  const setIsDone = (index: number, isDone: boolean) => {
    if (taskList == null) return;
    const newTaskList = {
      ...taskList,
      tasks: taskList.tasks.map((t, i) => (i === index ? { ...t, isDone } : t)),
    };
    taskListService
      .updateTaskList(newTaskList)
      .then((res) => {
        setTaskList(res);
        setDimmerActive(true);
      });
  };
  const doneTaskCount = taskList?.tasks.map((t) => t.isDone).length || 0;
  const taskCount = taskList?.tasks.length || 0;

  return (
    <div>
      {!taskList && <Dimmer active><Loader /></Dimmer>}
      {taskList && <TaskListBar taskList={taskList} setIsDone={setIsDone} finalize={finalize} />}
      <Dimmer active={dimmerActive} onClickOutside={() => setDimmerActive(false)} page>
        <Header as="h2" icon inverted>
          <Icon name="heart" />
          {'You\'re improving!'}
          <Header.Subheader>{`You've finished ${doneTaskCount} out of ${taskCount} tasks!`}</Header.Subheader>
        </Header>
      </Dimmer>
    </div>
  );
};

export default TaskListPage;
