const jwt = require('jsonwebtoken');
const config = require('../config');

const extractJWT = (req, res, next) => {
  const authorization = req.get('Authorization');
  if (authorization && authorization.toLowerCase().startsWith('bearer ')) {
    req.token = authorization.substring(7);
  }
  next();
};

const authorizeUser = (req, res, next) => {
  const { token } = req;
  if (token) {
    const decodedToken = jwt.verify(token, config.SECRET);
    req.userId = decodedToken.id ? decodedToken.id : null;
  } else {
    req.userId = null;
  }
  next();
};

module.exports = { extractJWT, authorizeUser };
