const TaskList = require('../model/taskList');
const { validObjectId } = require('../helpers/validators');
const { ErrorWrapper } = require('../helpers/error');

module.exports = {
  async createTaskList(body) {
    return TaskList.create(body);
  },

  async readTaskList(id) {
    if (!validObjectId(id)) {
      throw new ErrorWrapper(400, 'Invalid resource ID');
    }
    return TaskList.findById(id).populate('tasks');
  },

  async readTaskLists() {
    return TaskList.find({}).populate('tasks');
  },

  async readUserTaskLists(userId) {
    return TaskList.find({ userId }).populate('tasks').exec();
  },

  async updateTaskList(id, body) {
    if (!validObjectId(id)) {
      throw new ErrorWrapper(400, 'Invalid resource ID');
    }
    return TaskList.findByIdAndUpdate(id, body, { new: true });
  },

  async deleteTaskList(id) {
    if (!validObjectId(id)) {
      throw new ErrorWrapper(400, 'Invalid resource ID');
    }
    return TaskList.findByIdAndDelete(id);
  },
};
