const Task = require('../model/task');

module.exports = {
  async createTask(body) {
    return Task.create(body);
  },

  async readTask(id) {
    return Task.findById(id).populate('attributes');
  },

  async readTasks() {
    return Task.find({}).populate('attributes');
  },

  async updateTask(id, body) {
    return Task.findByIdAndUpdate(id, body, { new: true });
  },

  async deleteTask(id) {
    return Task.findByIdAndDelete(id);
  },
};
