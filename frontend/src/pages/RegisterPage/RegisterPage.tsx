import React, { useState } from 'react';
import { Button, Form, Segment } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';

import userService from '../../services/userService';

const RegisterPage: React.FC = () => {
  const history = useHistory();
  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const register = () => {
    userService
      .createUser(username, password)
      .then(() => {
        history.push('/login');
      });
  };

  return (
    <Segment style={{ width: '50%', verticalAlign: 'middle', margin: 'auto' }}>
      <Form>
        <Form.Field>
          <label>Username</label>
          <input
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Field>
        <Form.Field>
          <label>Password</label>
          <input
            placeholder="Password"
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Field>
        <Button
          type="submit"
          onClick={(e) => {
            e.preventDefault();
            register();
          }}
          primary
        >
          Register
        </Button>
      </Form>
    </Segment>
  );
};

export default RegisterPage;
