import React from 'react';
import { Header, Segment, Item } from 'semantic-ui-react';

const RankingsPage: React.FC = () => (
  <Segment style={{ width: '80%', verticalAlign: 'middle', margin: 'auto' }}>
    <Header>Rankings</Header>
    <Item.Group>
      <Item>
        <Item.Content>
          <Item.Header as="a">1. You</Item.Header>
          <Item.Meta>Level 1</Item.Meta>
          <Item.Description>{'Congrats you\'re first!'}</Item.Description>
        </Item.Content>
      </Item>
      <Item>
        <Item.Content>
          <Item.Header as="a">2. Your Friend</Item.Header>
          <Item.Meta>Level 0</Item.Meta>
          <Item.Description>{'You\'ve beaten your friend!'}</Item.Description>
        </Item.Content>
      </Item>
    </Item.Group>
  </Segment>
);

export default RankingsPage;
