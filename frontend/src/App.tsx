import React from 'react';
import {
  BrowserRouter as Router, Redirect, Route, Switch,
} from 'react-router-dom';

import LandingPage from './pages/LandingPage';
import TaskListPage from './pages/TaskListPage';
import LoginPage from './pages/LoginPage';
import MenuBar from './components/MenuBar';
import RegisterPage from './pages/RegisterPage';
import TaskListsPage from './pages/TaskListsPage';
import RankingsPage from './pages/RankingsPage';
import CreateTaskListPage from './pages/CreateTaskListPage';

const App: React.FC = () => (
  <Router>
    <div className="App">
      <Switch>
        <Route path="/lists/create">
          <MenuBar isLoggedIn />
          <CreateTaskListPage />
        </Route>
        <Route path="/lists/:id">
          <MenuBar isLoggedIn />
          <TaskListPage />
        </Route>
        <Route path="/lists/">
          <MenuBar isLoggedIn />
          <TaskListsPage />
        </Route>
        <Route path="/rankings/">
          <MenuBar isLoggedIn />
          <RankingsPage />
        </Route>
        <Route path="/profile">
          <MenuBar isLoggedIn />
          <LandingPage />
        </Route>
        <Route path="/login">
          <MenuBar isLoggedIn={false} />
          <LoginPage />
        </Route>
        <Route path="/register">
          <MenuBar isLoggedIn={false} />
          <RegisterPage />
        </Route>
        <Route>
          <Redirect to="/profile" />
        </Route>
      </Switch>
    </div>
  </Router>
);

export default App;
