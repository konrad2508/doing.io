import React from 'react';
import { Header, Segment } from 'semantic-ui-react';

import CreateTaskListForm from './CreateTaskListForm';

const CreateTaskListPage: React.FC = () => (
  <Segment style={{ width: '80%', verticalAlign: 'middle', margin: 'auto' }}>
    <Header>Create task list</Header>
    <CreateTaskListForm />
  </Segment>
);

export default CreateTaskListPage;
