const express = require('express');
const userService = require('../service/user');

const userController = express.Router();

userController.get('/', async (req, res) => {
  const users = await userService.readUsers();
  return res.status(200).json(users);
});

userController.post('/', async (req, res) => {
  const createdUser = await userService.createUser(req.body.username, req.body.password);
  return res.status(201).json(createdUser);
});

userController.get('/:id', (req, res, next) => {
  userService.readUser(req.params.id)
    .then((user) => {
      if (user === null) {
        return res.status(404).send();
      }
      return res.status(200).json(user);
    })
    .catch((err) => next(err));
});

userController.put('/:id', (req, res, next) => {
  userService.updateUser(req.params.id, req.body)
    .then((updatedUser) => {
      if (updatedUser === null) {
        return res.status(404).json(updatedUser);
      }
      return res.status(200).json(updatedUser);
    })
    .catch((err) => next(err));
});

userController.delete('/:id', (req, res, next) => {
  userService.deleteUser(req.params.id)
    .then((deletedUser) => {
      if (deletedUser === null) {
        return res.status(404).send();
      }
      return res.status(204).send();
    })
    .catch((err) => next(err));
});

module.exports = userController;
