const Plan = require('../model/plan');

module.exports = {
  async createPlan(body) {
    return Plan.create(body);
  },

  async readPlan(id) {
    return Plan.findById(id);
  },

  async readPlans() {
    return Plan.find({});
  },

  async readUserPlans(userId) {
    return Plan.find({ userId }).exec();
  },

  async updatePlan(id, body) {
    return Plan.findByIdAndUpdate(id, body, { new: true });
  },

  async deletePlan(id) {
    return Plan.findByIdAndDelete(id);
  },
};
