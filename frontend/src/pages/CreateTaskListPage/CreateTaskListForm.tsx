import React, { useState } from 'react';
import {
  Button, Form, Header, Icon, Segment,
} from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';

import { NewTask } from '../../types';
import TaskForm from './TaskForm';
import taskListService from '../../services/taskListService';

const CreateTaskListForm: React.FC = () => {
  const history = useHistory();
  const [name, setName] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [tasks, setTasks] = useState<NewTask[]>([]);

  const createTask = () => {
    const newTask : NewTask = {
      name: '', description: '', experience: 0, attributes: { strength: 0, intellect: 0 }, isDone: false,
    };
    setTasks(tasks.concat(newTask));
  };
  const updateTask = (index: number, task: NewTask) => {
    setTasks(tasks.map((t, i) => (index === i ? task : t)));
  };

  const createTaskList = () => {
    taskListService
      .createTaskList({
        name, description, tasks, isFinalized: false,
      })
      .then(() => history.push('/profile'));
  };

  return (
    <Form>
      <Form.Field>
        <label>Name</label>
        <input placeholder="Name" value={name} onChange={(e) => setName(e.target.value)} />
      </Form.Field>
      <Form.Field>
        <label>Description</label>
        <input placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)} />
      </Form.Field>
      <Segment>
        <Header>Tasks</Header>
        {tasks.map((task, index) => (<TaskForm task={task} updateTask={(t) => updateTask(index, t)} />))}
        <Button onClick={() => createTask()} icon fluid><Icon name="plus" color="grey" fitted /></Button>
      </Segment>
      <Button type="submit" primary fluid onClick={(e) => { e.preventDefault(); createTaskList(); }}>Submit</Button>
    </Form>
  );
};

export default CreateTaskListForm;
