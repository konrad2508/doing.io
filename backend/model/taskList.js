const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const taskListSchema = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  tasks: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Task' }],
  isFinalized: { type: Boolean, required: true },
});
taskListSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = document._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
  },
});
taskListSchema.plugin(uniqueValidator);

module.exports = mongoose.model('TaskList', taskListSchema);
