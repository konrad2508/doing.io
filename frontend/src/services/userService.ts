import axios from 'axios';

import { User } from '../types';
import { SERVER_ADDRESS } from '../config';

const BASE_URL = `${SERVER_ADDRESS}/users`;
const getToken: () => string | null = () => localStorage.getItem('user_token');

const getUsers = (): Promise<User[]> => axios
  .get(`${BASE_URL}/`)
  .then((r) => r.data as User[]);

const getUser = (id: string): Promise<User> => axios
  .get(`${BASE_URL}/${id}`, { headers: { Authorization: getToken() ? `bearer ${getToken()}` : '' } })
  .then((r) => r.data as User);

const createUser = (username: string, password: string): Promise<User> => axios
  .post(`${BASE_URL}/`, { username, password }, { headers: { Authorization: getToken() ? `bearer ${getToken()}` : '' } })
  .then((r) => r.data as User);

export default {
  getUsers,
  getUser,
  createUser,
};
