import axios from 'axios';

import { NewTaskList, TaskList } from '../types';
import { SERVER_ADDRESS } from '../config';

const BASE_URL = `${SERVER_ADDRESS}/lists`;
const getToken: () => string | null = () => localStorage.getItem('user_token');

const getTaskLists = (): Promise<TaskList[]> => axios
  .get(`${BASE_URL}/`)
  .then((r) => r.data as TaskList[]);

const getTaskList = (id: string): Promise<TaskList> => axios
  .get(`${BASE_URL}/${id}`, { headers: { Authorization: getToken() ? `bearer ${getToken()}` : '' } })
  .then((r) => r.data as TaskList);

const createTaskList = (taskList: NewTaskList): Promise<TaskList> => axios
  .post(`${BASE_URL}/`, taskList, { headers: { Authorization: getToken() ? `bearer ${getToken()}` : '' } })
  .then((r) => r.data as TaskList);

const updateTaskList = (taskList: TaskList): Promise<TaskList> => axios
  .put(`${BASE_URL}/${taskList.id}`, taskList, { headers: { Authorization: getToken() ? `bearer ${getToken()}` : '' } })
  .then((r) => r.data as TaskList);

export default {
  getTaskLists,
  getTaskList,
  createTaskList,
  updateTaskList,
};
