import React from 'react';
import { Card } from 'semantic-ui-react';

import { TaskList } from '../../types';
import TaskListCard from './TaskListCard';

interface TaskListsBarProps {
    taskLists: TaskList[]
}

const TaskListsBar: React.FC<TaskListsBarProps> = ({ taskLists }) => (
  <Card.Group itemsPerRow={4}>
    {taskLists.map((tl) => (<TaskListCard taskList={tl} />))}
  </Card.Group>
);

export default TaskListsBar;
