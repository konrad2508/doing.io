const express = require('express');
const { ObjectId } = require('mongoose').Types;
const taskListService = require('../service/taskList');
const userService = require('../service/user');

const TaskList = require('../model/taskList');

const taskListController = express.Router();

taskListController.get('/', async (req, res) => {
  const taskLists = await taskListService.readTaskLists();
  return res.status(200).json(taskLists);
});

taskListController.post('/', async (req, res) => {
  const createdTaskList = await taskListService.createTaskList(req.body);
  return res.status(201).json(createdTaskList);
});

taskListController.get('/:id', (req, res, next) => {
  taskListService.readTaskList(req.params.id)
    .then((taskList) => {
      if (taskList === null) {
        return res.status(404).send();
      }
      return res.status(200).json(taskList);
    })
    .catch((err) => next(err));
});

taskListController.get('/user/:userId', async (req, res, next) => {
  const taskList = await taskListService.readUserTaskLists(req.params.userId)
    .catch((err) => next(err));

  if (taskList === null) {
    return res.status(404).send();
  }
  return res.status(200).json(taskList);
});

taskListController.put('/:id', (req, res, next) => {
  taskListService.updateTaskList(req.params.id, req.body)
    .then((updatedTaskList) => {
      if (updatedTaskList === null) {
        return res.status(404).send();
      }
      return res.status(200).json(updatedTaskList);
    })
    .catch((err) => next(err));
});

taskListController.delete('/:id', (req, res, next) => {
  taskListService.deleteTaskList(req.params.id)
    .then((deletedTaskList) => {
      if (deletedTaskList === null) {
        return res.status(404).send();
      }
      return res.status(204).send();
    })
    .catch((err) => next(err));
});

taskListController.post('/:id', async (req, res) => {
  const user = await userService.readUserById(req.userId);
  if (user === null) return res.status(404).send();

  const taskListToCopy = await taskListService.readTaskList(req.params.id);
  if (taskListToCopy === null) return res.status(404).send();

  const copiedTaskList = await (new TaskList({
    name: taskListToCopy.name,
    description: taskListToCopy.description,
    tasks: taskListToCopy.tasks,
    isFinalized: false,
  }).save());

  user.taskLists.push(ObjectId(copiedTaskList.id));

  userService.updateUser(req.userId, user)
    .then((updatedUser) => res.status(201).json(updatedUser));
});

module.exports = taskListController;
