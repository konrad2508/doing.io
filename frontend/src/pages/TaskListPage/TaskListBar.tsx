import React from 'react';
import {
  Button, Icon, Item, Segment,
} from 'semantic-ui-react';

import { TaskList } from '../../types';
import TaskItem from './TaskItem';

interface TaskListCardProps {
    taskList: TaskList
    setIsDone: (arg0: number, arg1: boolean) => void
    finalize: () => void
}

const TaskListCard: React.FC<TaskListCardProps> = ({ taskList, setIsDone, finalize }) => (
  <Segment>
    <Item.Group>
      <Item>
        <Item.Content>
          <Item.Header as="h1" content={taskList.name} />
          <Button floated="right" primary icon disabled={taskList.isFinalized} onClick={() => finalize()}>
            {!taskList.isFinalized && <Icon name="check" />}
            {taskList.isFinalized ? 'Finalized' : ' Finalize'}
          </Button>
          <Item.Description>
            {taskList.description}
          </Item.Description>
        </Item.Content>
      </Item>
    </Item.Group>
    <Segment>
      <Item.Group divided link={!taskList.isFinalized}>
        {taskList.tasks.map((task, index) => (
          <TaskItem
            task={task}
            setIsDone={(isDone) => setIsDone(index, isDone)}
            isFinalized={taskList.isFinalized}
          />
        ))}
      </Item.Group>
    </Segment>
  </Segment>
);

export default TaskListCard;
