const express = require('express');
const taskService = require('../service/task');

const taskController = express.Router();

taskController.get('/', async (req, res) => {
  const tasks = await taskService.readTasks();
  return res.status(200).json(tasks);
});

taskController.post('/', async (req, res) => {
  const createdTask = await taskService.createTask(req.body);
  return res.status(201).json(createdTask);
});

taskController.get('/:id', (req, res, next) => {
  taskService.readTask(req.params.id)
    .then((task) => {
      if (task === null) {
        return res.status(404).send();
      }
      return res.status(200).json(task);
    })
    .catch((err) => next(err));
});

taskController.put('/:id', (req, res, next) => {
  taskService.updateTask(req.params.id, req.body)
    .then((updatedTask) => {
      if (updatedTask === null) {
        return res.status(404).send();
      }
      return res.status(200).json(updatedTask);
    })
    .catch((err) => next(err));
});

taskController.delete('/:id', (req, res, next) => {
  taskService.deleteTask(req.params.id)
    .then((deletedTask) => {
      if (deletedTask === null) {
        return res.status(404).send();
      }
      return res.status(204).send();
    })
    .catch((err) => next(err));
});

module.exports = taskController;
