const loginController = require('express').Router();
const loginService = require('../service/login');
const userService = require('../service/user');

loginController.post('/login', async (req, res) => {
  const { user, validPassword } = await loginService.checkCredentials(req.body.username, req.body.password);
  if (!user || !validPassword) return res.status(401).send();
  const token = loginService.authUser(user);
  return res.status(200).send({ token });
});

loginController.get('/me', async (req, res) => {
  const user = await userService.readUserById(req.userId);
  if (!user) return res.status(401).send();
  return res.status(200).json(user);
});
module.exports = loginController;
