const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const taskSchema = new mongoose.Schema({
  name: { type: String, required: true },
  experience: { type: Number, required: true },
  description: { type: String, required: true },
  attributes: { type: mongoose.Schema.Types.ObjectId, ref: 'Attribute', required: true },
  isDone: { type: Boolean, required: true },
});
taskSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = document._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
  },
});
taskSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Task', taskSchema);
