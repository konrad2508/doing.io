const bcrypt = require('bcrypt');

const User = require('../model/user');
const Attribute = require('../model/attribute');

module.exports = {
  async createUser(username, password) {
    const saltRounds = 10;
    const passwordHash = await bcrypt.hash(password, saltRounds);

    const attribute = await new Attribute({
      strength: 0,
      intellect: 0,
    }).save();

    return new User({
      username,
      passwordHash,
      level: 1,
      experience: 0,
      attributes: attribute._id,
      taskLists: [],
    }).save();
  },

  async readUser(username) {
    return User
      .findOne({ username })
      .populate('attributes')
      .populate('taskLists');
  },

  async readUserById(id) {
    return User
      .findById(id)
      .populate('attributes')
      .populate('taskLists');
  },

  async readUsers() {
    return User
      .find({})
      .sort([['level', -1]])
      .sort([['experience', -1]])
      .populate('attributes')
      .populate('taskLists');
  },

  async updateUser(id, body) {
    return User
      .findByIdAndUpdate(id, body, { new: true })
      .populate('attributes')
      .populate('taskLists');
  },

  async deleteUser(id) {
    return User.findByIdAndDelete(id);
  },
};
