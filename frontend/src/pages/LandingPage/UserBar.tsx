import React from 'react';
import { Item, Progress, Segment } from 'semantic-ui-react';

import { User } from '../../types';
import UserAttribute from '../../components/UserAttribute';

interface UserBarProps {
    user: User
}

const imageSrc = 'https://i.pinimg.com/236x/cc/a9/ea/cca9ea436c3d0071bd31b893fd37543d--nick-bateman-drinking-coffee.jpg';

const UserBar: React.FC<UserBarProps> = ({ user }) => (
  <Segment>
    <Item.Group>
      <Item>
        <Item.Image src={imageSrc} size="small" />
        <Item.Content>
          <Item.Header as="h1" content={user.username} />
          <Item.Meta>
            <span className="level">{`Level: ${user.level}`}</span>
          </Item.Meta>
          <Item.Description>
            <Progress value={user.experience} total={100} progress="percent" warning>
              Experience
            </Progress>
          </Item.Description>
          <Item.Extra>
            <UserAttribute icon="heart" name="Strength" level={user.attributes.strength} color="red" />
            <UserAttribute icon="book" name="Intellect" level={user.attributes.intellect} color="blue" />
          </Item.Extra>
        </Item.Content>
      </Item>
    </Item.Group>
  </Segment>
);

export default UserBar;
