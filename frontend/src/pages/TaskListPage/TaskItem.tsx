import React from 'react';
import { Button, Item } from 'semantic-ui-react';

import { Task } from '../../types';
import UserAttribute from '../../components/UserAttribute';

interface TaskItemProps {
    task: Task
    setIsDone: (arg0: boolean) => void
    isFinalized: boolean
}

const TaskItem: React.FC<TaskItemProps> = ({ task, setIsDone, isFinalized }) => (
  <Item
    onClick={() => setIsDone(isFinalized ? task.isDone : !task.isDone)}
  >
    <Item.Content verticalAlign="middle">
      <Item.Header>{task.name}</Item.Header>
      <Item.Meta>{`+ ${task.experience} EXP`}</Item.Meta>
      <Item.Description>{task.description}</Item.Description>
      <Item.Extra>
        {task.attributes.strength > 0
                && <UserAttribute icon="heart" name="Strength" level={task.attributes.strength} color="red" />}
        {task.attributes.intellect > 0
                && <UserAttribute icon="book" name="Intellect" level={task.attributes.intellect} color="blue" />}
        {task.isDone
                && <Button floated="right" color="grey" compact disabled={isFinalized}>TODO</Button>}
        {!task.isDone
                && <Button floated="right" color="green" compact disabled={isFinalized}>DONE</Button>}
      </Item.Extra>
    </Item.Content>
  </Item>
);

export default TaskItem;
