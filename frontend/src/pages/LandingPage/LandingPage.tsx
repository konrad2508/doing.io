import React, { useEffect, useState } from 'react';
import {
  Dimmer, Header, Loader, Segment,
} from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';

import UserBar from './UserBar';
import TaskListsBar from './TaskListsBar';
import authService from '../../services/authService';
import { User } from '../../types';

const LandingPage: React.FC = () => {
  const history = useHistory();
  const [user, setUser] = useState<User | null>(null);

  useEffect(() => {
    authService
      .me()
      .then((res) => setUser(res))
      .catch(() => {
        localStorage.removeItem('user_token');
        history.push('/login');
      });
  }, [history]);

  return (
    <Segment style={{ width: '80%', verticalAlign: 'middle', margin: 'auto' }}>
      {!user && <Dimmer active><Loader /></Dimmer>}
      {user && (
        <>
          <UserBar user={user} />
          <Segment>
            <Header>My lists of tasks:</Header>
          </Segment>
          <TaskListsBar taskLists={user.taskLists} />
        </>
      )}
    </Segment>
  );
};

export default LandingPage;
