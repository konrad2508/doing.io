const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('./config');

const { extractJWT, authorizeUser } = require('./helpers/middleware');

const loginController = require('./controller/login');
const userController = require('./controller/user');
const taskController = require('./controller/task');
const taskListController = require('./controller/taskList');

const app = express();
const port = 3001;

const mongoUrl = config.MONGODB_URI;
mongoose.connect(mongoUrl, { useNewUrlParser: true });

app.use(cors());
app.use(express.json());
app.use(extractJWT);
app.use(authorizeUser);

app.use('/api/auth', loginController);
app.use('/api/users', userController);
app.use('/api/task', taskController);
app.use('/api/lists', taskListController);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
