const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const attributeSchema = new mongoose.Schema({
  strength: { type: Number, required: true },
  intellect: { type: Number, required: true },
});
attributeSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = document._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
  },
});
attributeSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Attribute', attributeSchema);
