import React, { useEffect, useState } from 'react';
import { Header, Segment } from 'semantic-ui-react';

import TaskListsBar from '../LandingPage/TaskListsBar';
import taskListService from '../../services/taskListService';
import { TaskList } from '../../types';

const TaskListsPage: React.FC = () => {
  const [taskLists, setTokenLists] = useState<TaskList[]>([]);

  useEffect(() => {
    taskListService
      .getTaskLists()
      .then((tl) => setTokenLists(tl));
  }, []);

  return (
    <Segment style={{ width: '80%', verticalAlign: 'middle', margin: 'auto' }}>
      <Header>Lists of tasks</Header>
      <TaskListsBar taskLists={taskLists} />
    </Segment>
  );
};

export default TaskListsPage;
