import React, { useState } from 'react';
import { Button, Form, Segment } from 'semantic-ui-react';
import { Link, useHistory } from 'react-router-dom';

import authService from '../../services/authService';

const LoginPage: React.FC = () => {
  const history = useHistory();
  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const login = () => {
    authService
      .login(username, password)
      .then((res) => {
        localStorage.setItem('user_token', res.token);
        history.push('/');
      });
  };

  return (
    <Segment style={{ width: '50%', verticalAlign: 'middle', margin: 'auto' }}>
      <Segment>
        <Form>
          <Form.Field>
            <label>Username</label>
            <input
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </Form.Field>
          <Form.Field>
            <label>Password</label>
            <input
              placeholder="Password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Field>
          <Button
            type="submit"
            onClick={(e) => {
              e.preventDefault();
              login();
            }}
          >
            Login
          </Button>
        </Form>
      </Segment>
      <div style={{ textAlign: 'center' }}>
        {'Don\'t have an account? Register '}
        <Link to="/register">here</Link>
        .
      </div>
    </Segment>
  );
};

export default LoginPage;
