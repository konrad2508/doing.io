import React from 'react';
import { Card } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';

import { TaskList } from '../../types';

interface TaskListCardProps {
    taskList: TaskList
}

const TaskListCard: React.FC<TaskListCardProps> = ({ taskList }) => {
  const history = useHistory();
  const doneTaskCount = taskList.tasks.filter((t) => t.isDone).length;
  const taskListDone = doneTaskCount === taskList.tasks.length;

  return (
    <Card onClick={() => history.push(`/lists/${taskList.id}`)} color={taskListDone ? 'green' : 'grey'}>
      <Card.Content>
        <Card.Header content={taskList.name} />
        <Card.Meta content={`Done: ${doneTaskCount}/${taskList.tasks.length}`} />
        <Card.Description content={taskList.description} />
      </Card.Content>
    </Card>
  );
};

export default TaskListCard;
