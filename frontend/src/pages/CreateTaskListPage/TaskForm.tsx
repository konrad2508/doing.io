import React from 'react';
import { Form, Segment } from 'semantic-ui-react';
import { NewTask } from '../../types';

interface TaskFormProps {
    task: NewTask
    updateTask: (task:NewTask) => void
}

const TaskForm: React.FC<TaskFormProps> = ({ task, updateTask }) => (
  <Segment>
    <Form>
      <Form.Field inline>
        <label>Name</label>
        <input
          placeholder="Name"
          value={task.name}
          onChange={(e) => updateTask({ ...task, name: e.target.value })}
        />
      </Form.Field>
      <Form.Field inline>
        <label>Description</label>
        <input
          placeholder="Description"
          value={task.description}
          onChange={(e) => updateTask({ ...task, description: e.target.value })}
        />
      </Form.Field>
      <Form.Field>
        <label>{`Experience: ${task.experience}`}</label>
        <input
          placeholder="Experience"
          min="0"
          max="100"
          type="range"
          value={task.experience}
          onChange={(e) => updateTask({ ...task, experience: Number(e.target.value) })}
        />
      </Form.Field>
      <Form.Field>
        <label>{`Strength: ${task.attributes.strength}`}</label>
        <input
          placeholder="strength"
          min="0"
          max="10"
          type="range"
          value={task.attributes.strength}
          onChange={(e) => (
            updateTask({ ...task, attributes: { ...task.attributes, strength: Number(e.target.value) } })
          )}
        />
      </Form.Field>
      <Form.Field>
        <label>{`Intellect: ${task.attributes.intellect}`}</label>
        <input
          placeholder="intellect"
          min="0"
          max="10"
          type="range"
          value={task.attributes.intellect}
          onChange={(e) => (
            updateTask({ ...task, attributes: { ...task.attributes, intellect: Number(e.target.value) } })
          )}
        />
      </Form.Field>
    </Form>
  </Segment>
);

export default TaskForm;
