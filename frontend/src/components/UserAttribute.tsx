import React from 'react';
import {
  Icon, Label, SemanticCOLORS, SemanticICONS,
} from 'semantic-ui-react';

interface UserAttributeProps {
    icon: SemanticICONS
    color: SemanticCOLORS
    name: string
    level: number
}

const UserAttribute: React.FC<UserAttributeProps> = ({
  icon, color, name, level,
}) => (
  <Label size="large" color={color}>
    <Icon name={icon} />
    {`${name}: ${level}`}
  </Label>
);

export default UserAttribute;
