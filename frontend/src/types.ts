export interface NewAttributes {
    strength: number
    intellect: number
}

export interface NewTask {
    name: string
    experience: number
    description: string
    attributes: NewAttributes
    isDone: boolean
}

export interface NewTaskList {
    name: string
    description: string
    tasks: NewTask[]
    isFinalized: boolean
}

export interface Attributes {
    id: string
    strength: number
    intellect: number
}

export interface Task {
    id: string
    name: string
    experience: number
    description: string
    attributes: Attributes
    isDone: boolean
}

export interface TaskList {
    id: string
    name: string
    description: string
    tasks: Task[]
    isFinalized: boolean
}

export interface User {
    id: string
    username: string
    level: number
    attributes: Attributes
    experience: number
    taskLists: TaskList[]
}

export interface UserToken {
    token: string
}
