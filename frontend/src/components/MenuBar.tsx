import React from 'react';
import { Menu } from 'semantic-ui-react';
import { useHistory, useLocation } from 'react-router-dom';

interface MenuBarProps {
    isLoggedIn: boolean
}

const MenuBar: React.FC<MenuBarProps> = ({ isLoggedIn }) => {
  const history = useHistory();
  const location = useLocation();

  return (
    <Menu>
      <Menu.Item
        name="logo"
      >
        doing.io
      </Menu.Item>
      {isLoggedIn && (
        <Menu.Item
          name="profile"
          active={location.pathname === '/profile'}
          onClick={() => history.push('/profile')}
        >
          Profile
        </Menu.Item>
      )}
      {isLoggedIn && (
        <Menu.Item
          name="lists"
          active={location.pathname === '/lists'}
          onClick={() => history.push('/lists')}
        >
          Tasks Lists
        </Menu.Item>
      )}
      {isLoggedIn && (
        <Menu.Item
          name="tasklist"
          active={location.pathname === '/lists/create'}
          onClick={() => history.push('/lists/create')}
        >
          Create Tasks List
        </Menu.Item>
      )}
      {isLoggedIn && (
        <Menu.Item
          name="rankings"
          active={location.pathname === '/rankings'}
          onClick={() => history.push('/rankings')}
        >
          Rankings
        </Menu.Item>
      )}
      {isLoggedIn && (
        <Menu.Item
          name="logout"
          onClick={() => { localStorage.removeItem('user_token'); history.push('/login'); }}
          position="right"
        >
          Log out
        </Menu.Item>
      )}
    </Menu>
  );
};

export default MenuBar;
