class ErrorWrapper extends Error {
  constructor(statusCode, message) {
    super();
    this.statusCode = statusCode;
    this.message = message;
  }
}

const handleError = (err, res) => {
  var { statusCode, message } = err;

  if (!statusCode) {
    statusCode = 500;
  }

  res.status(statusCode).json({
    status: 'error',
    statusCode,
    message,
  });
};

module.exports = {
  ErrorWrapper,
  handleError,
};
